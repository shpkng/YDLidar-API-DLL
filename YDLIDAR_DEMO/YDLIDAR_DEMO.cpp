// YdLidar_demo.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include "lidar.h"
#include <windows.h> 
#include <iostream>
#include <cmath>
#include <cstdlib>
#include <atlstr.h>

#pragma region 坐标转换用的各个参数

//极坐标到直角坐标
//a角是结果角度，d是*4的距离
//d0 = d*4       !!!!!!!!!!!!!!!!!!!!!!!!!!!
/*
				·→  (0, ∞)

	·································   (a0, d0)
	|						  |
	|						  |
	|						  |
	|						  |
	·································   (a1, d1)

*/


float a0 = 40;
float d0 = 1440;
float a1 = 72;
float d1 = 10000;


float widthHalf = d0 * cos(a0);
float heightLow = d0 * sin(a0);
float heightHigh = d1 * sin(a1);

//直角坐标到屏幕坐标
float xBias;
float xScale;
float yBias;
float yScale;

#pragma endregion

//
bool isBufferZeroOccupied = true;

//一个buffer由x数组y数组和一个长度组成
float bufferX[1024];
float  bufferY[1024];
int bufferLen;


int flag = 7263;
bool bLoop = false;

YDlidarDriver * lidar_drv;

//float oriX = -d1 * cos(a1);
//float oriY = -d1 * sin(a1);
//
//float width = -2 * oriX;
//float height = -d0 * sin(a0) - oriY;



void Scan();


bool ctrlhandler(DWORD fdwctrltype) {
	switch (fdwctrltype) {
	case CTRL_CLOSE_EVENT:
		Lidar::GetInstance().onDisconnect();
		break;
	}
	return TRUE;
}

int main()

//激光雷达的主循环
//extern "C" _declspec(dllexport) void _stdcall StartRaidar()
{
	flag = 3627;
	bLoop = true;

	char serialpath[255] = "\\\\.\\com6";
	uint32_t baudrate = 128000;

#pragma region 废话
	printf("EAI INFO : start to open the serialport......\n");
	bool isConnected = Lidar::GetInstance().onConnect(serialpath, baudrate);
	if (!isConnected) {
		printf("EAI INFO : cannot open the serialport......\n");
		Sleep(5 * 1000);
	}
	printf("EAI INFO :it is ok to open the serialport......\n");

	printf("EAI INFO : start to check health......\n");
	int errorcode;
	if (!Lidar::GetInstance().checkDeviceHealth(&errorcode)) {
		printf("EAI INFO : the health is error.....\n");
		Sleep(5 * 1000);
	}
	printf("EAI INFO : health is ok ......\n");
#pragma endregion


	lidar_drv = Lidar::GetInstance().lidar_drv;

	lidar_drv->startScan();
	printf("EAI INFO : scaning......\n");
	if (!SetConsoleCtrlHandler((PHANDLER_ROUTINE)ctrlhandler, true)) {
		printf("Something wrong");
	}

	while (true)
		Scan();
}

void Scan() {
	node_info nodes[2048];	//点云buffer数组
	size_t cnt = sizeof(nodes);

	if (lidar_drv->grabScanData(nodes, cnt, 0) == 0) {

		lidar_drv->ascendScanData(nodes, cnt);

		cout << cnt << endl;
		int j = 0;

		for (int i = 0; i < cnt; i++) {

			float angle = (nodes[i].angle_q6_checkbit >> LIDAR_RESP_MEASUREMENT_ANGLE_SHIFT) / 64.0f;

			angle += atan(21.8* (155.3 - nodes[i].distance_q2 / 4) / (155.3*nodes[i].distance_q2 / 4));


			cout << angle << "   " << nodes[i].distance_q2 << endl;
			//剔除距离不符合的点
			if (nodes[i].distance_q2 == 0)
				//if(nodes[i].distance_q2==0)
			{
				cout << "Too near" << endl;
				continue;
			}

			if (nodes[i].distance_q2 > d1)
			{
				cout << "Too far" << endl;
				continue;
			}



			//剔除角度不符合的点
			if (angle < a0 || angle>180 - a0)
			{
								cout << "wrong angle" << endl;
				continue;
			}

			float pX = nodes[i].distance_q2 * cos(angle);
			float pY = nodes[i].distance_q2 * sin(angle);

			//剔除不在屏幕上的点
			if (abs(pX) > widthHalf || pY > heightHigh || pY < heightLow)
			{
				cout << "wrong position   " << (bool)(abs(pX) > widthHalf) << "   " << (bool)(pY > heightHigh) << endl;
				continue;
			}

			//转换到屏幕坐标
			pX = pX / 4 * xScale + xBias;
			pY = pY / 4 * yScale + yBias;

			//写入数组
			bufferX[j] = pX;
			bufferY[j++] = pY;
		}
		bufferLen = j + 1;

		cout << j << endl;
		bufferX[1023] = 30;
		bufferY[1023] = 30;
	}
}

//extern "C" _declspec(dllexport) void _stdcall ScanOnce() {
//	node_info nodes[2048];	//点云buffer数组
//	size_t cnt = sizeof(nodes);
//
//
//	if (lidar_drv->grabScanData(nodes, cnt, 0) == 0) {
//
//		lidar_drv->ascendScanData(nodes, cnt);
//
//		int j = 0;
//
//		for (int i = 0; i < cnt; i++) {
//
//			//剔除距离不符合的点
//			if (nodes[i].distance_q2 == 0 || nodes[i].distance_q2 > d1)
//				continue;
//
//			float angle = (nodes[i].angle_q6_checkbit >> LIDAR_RESP_MEASUREMENT_ANGLE_SHIFT) / 64.0f;
//
//			angle += atan(21.8* (155.3 - nodes[i].distance_q2 / 4) / (155.3*nodes[i].distance_q2 / 4));
//
//			//剔除角度不符合的点
//			if (angle < a0 || angle>180 - a0)
//				continue;
//
//			float pX = nodes[i].distance_q2 * cos(angle);
//			float pY = nodes[i].distance_q2 * sin(angle);
//
//			//剔除不在屏幕上的点
//			if (abs(pX) > widthHalf || pY > heightHigh || pY < heightLow)
//				continue;
//
//			//转换到屏幕坐标
//			pX = pX / 4 * xScale + xBias;
//			pY = pY / 4 * yScale + yBias;
//
//			//写入数组
//			bufferX[j] = pX;
//			bufferY[j++] = pY;
//		}
//		bufferLen = j + 1;
//
//		bufferX[1023] = 30;
//		bufferY[1023] = 30;
//	}
//}

extern "C" _declspec(dllexport) float* _stdcall GetAxisX()
{
	return bufferX;
}

extern "C" _declspec(dllexport) float* _stdcall GetAxisY()
{
	return bufferY;
}

extern "C" _declspec(dllexport) int _stdcall GetBufferLength() {
	return bufferLen;
}

extern "C" _declspec(dllexport) void _stdcall Polar2Rect(float _a0, float _d0, float _a1, float _d1) {
	a0 = _a0;
	d0 = _d0;
	a1 = _a1;
	d1 = _d1;

}

extern "C" _declspec(dllexport) void _stdcall Rect2Screen(float _xBias, float _xScale, float _yBias, float _yScale)
{
	xBias = _xBias;
	xScale = _xScale;
	yBias = _yBias;
	yScale = _yScale;
}